#!/usr/bin/env python3

"""
Reproducer
"""

import warnings
import os

import gi


gi.require_version("Atspi", "2.0")
from gi.repository import Atspi
from gi.repository import GLib


warnings.filterwarnings("ignore", "g_object_unref")


class Registry:
    def __init__(self):
        self.main_loop = GLib.MainLoop()
        self._set_registry()

    def __call__(self):
        return self

    def _set_registry(self):
        self.event_listeners = dict()

    def get_desktop(self, numbered_desktop):
        return Atspi.get_desktop(numbered_desktop)


# Find a single satisfying descendant
# ----------------------------------------------------------------
def find_descendant(accesible_node, predicate):
    return _find_descendant_depth_first(accesible_node, predicate)


def _find_descendant_depth_first(accesible_node, predicate):
    if predicate(accesible_node):
        return accesible_node

    for child_at_index in range(len(accesible_node.children)):
        try:
            success_match = _find_descendant_depth_first(
                accesible_node.get_child_at_index(child_at_index), predicate
            )
        except Exception:
            success_match = None

        if success_match is not None:
            return success_match


# Find all satisfying descendants
# ----------------------------------------------------------------
def find_all_descendants(accessible_node, predicate):
    match_list = []
    _find_all_descendants(accessible_node, predicate, match_list)
    return match_list


def _find_all_descendants(accessible_node, predicate, match_list):
    for child_at_index in range(len(accessible_node.children)):
        try:
            if predicate(accessible_node.get_child_at_index(child_at_index)):
                match_list.append(accessible_node.get_child_at_index(child_at_index))
        except Exception as error:
            print(f"Debug log to be deleted: {error}")
        _find_all_descendants(accessible_node.get_child_at_index(child_at_index), predicate, match_list)


class Node:
    def find_child(self, predicate):
        result = None

        for _ in range(20):  # fixed cutoff in reproducer
            result = find_descendant(self, predicate)

            if result:
                break

        return result

    def find_children(self, predicate):
        return find_all_descendants(self, predicate)


class Root(Node):
    def application(self, application_name):
        return self.find_child(
            lambda x: x.name == application_name and x.get_role_name() == "application"
        )


Atspi.Accessible.__bases__ = (
    Root,
    Node,
) + Atspi.Accessible.__bases__


registry = Registry()
root = registry.get_desktop(0)


application = "gnome-control-center"
os.environ["DISPLAY"] = ":0"

accessibility_root = root.application(application)
if accessibility_root:
    accessibility_node = accessibility_root.get_child_at_index(0)
    print([x.name for x in accessibility_node.find_children(lambda y: "View" in y.name or "View" in y.description)])
else:
    print("Application was not loaded, the object is None")
